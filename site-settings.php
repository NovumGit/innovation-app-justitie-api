<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.jenv',
    'namespace'   => 'ApiNovumJenv',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'api.justitie.demo.novum.nu',
    'test_domain' => 'api.test.justitie.demo.novum.nu',
    'dev_domain'  => 'api.justitie.innovatieapp.nl',
];
